# kas - setup tool for bitbake based projects
#
# Copyright (c) Siemens AG, 2017-2018
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""
    The for-all-repos plugin for kas.
"""

import logging
import os
import subprocess
from kas.context import create_global_context
from kas.config import Config
from kas.libcmds import (Macro, Command, SetupDir, CleanupSSHAgent,
                         SetupSSHAgent, SetupEnviron,
                         WriteBBConfig, SetupHome, ReposApplyPatches,
                         Loop, InitSetupRepos, FinishSetupRepos,
                         SetupReposStep)

__license__ = 'MIT'
__copyright__ = 'Copyright (c) Siemens AG, 2017-2018'


class ForAllRepos:
    """
        This class implements the for-all-repos plugin for kas.
    """

    name = 'for-all-repos'
    helpmsg = (
        'Runs a specified command in all checked out repositories.'
    )

    @classmethod
    def setup_parser(cls, parser):
        """
            Setup the argument parser for the forall plugin
        """

        parser.add_argument('command',
                            help='Command to be executed as a string.')

    def run(self, args):
        """
            Executes the forall command of the kas plugin.
        """

        ctx = create_global_context(args)
        ctx.config = Config(args.config)

        macro = Macro()

        # Prepare
        macro.add(SetupDir())

        if 'SSH_PRIVATE_KEY' in os.environ:
            macro.add(SetupSSHAgent())

        macro.add(InitSetupRepos())

        repo_loop = Loop('repo_setup_loop')
        repo_loop.add(SetupReposStep())

        macro.add(repo_loop)
        macro.add(FinishSetupRepos())

        macro.add(SetupEnviron())
        macro.add(SetupHome())
        macro.add(ReposApplyPatches())

        macro.add(WriteBBConfig())

        macro.add(ForAllReposCommand(args.command))

        if 'SSH_PRIVATE_KEY' in os.environ:
            macro.add(CleanupSSHAgent())

        macro.run(ctx, args.skip)

        return True


class ForAllReposCommand(Command):
    """
        Implements the for-all-repos command.
    """

    def __init__(self, command):
        super().__init__()
        self.command = command

    def __str__(self):
        return 'for-all-repos'

    def execute(self, ctx):
        """
            Executes the chosen command in all checked out repositories.
        """

        for repo in ctx.config.get_repos():
            logging.info('%s$ %s', repo.path, self.command)
            retcode = subprocess.call(self.command, shell=True, cwd=repo.path,
                                     env=ctx.environ)
            if retcode != 0:
                logging.error('Command failed with return code %d', retcode)


__KAS_PLUGINS__ = [ForAllRepos]
